from django.urls import path
from . import views

urlpatterns = [
	path("", views.index, name="index"),
	path('ajout_etudiant', views.ajout_etudiant , name= 'saisie_etudiant' ),
]
