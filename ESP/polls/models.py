import datetime
from django.db import models
from django.utils import timezone
from django.urls import reverse

class Etudiant(models.Model):
    nom = models.CharField(max_length=100)
    prenom = models.CharField(max_length=100)
    date_naissance = models.DateField()
    lieu_naissance = models.CharField(max_length=100)
    pays_naissance = models.CharField(max_length=100)
    departement = models.CharField(max_length=100)
    voie_acces = models.CharField(max_length=100)
    matricule = models.CharField(max_length=100)
    nationalite = models.CharField(max_length=100)
    identifiant_national = models.CharField(max_length=100)
    statut = models.CharField(max_length=100)
    annee_inscription = models.CharField(max_length=100)
    def get_absolute_url(self):
    	return reverse('saisie_etudiant')


