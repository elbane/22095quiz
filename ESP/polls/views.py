from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Etudiant

def index(request):
	return render(request,'polls/index.html')
def ajout_etudiant(request):
    if request.method == 'POST':
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        matricule = request.POST['matricule']
        etudiant = Etudiant(
            nom=nom,
            prenom=prenom,       
            matricule=matricule,
        ) 
        etudiant.save()
        return redirect('liste_etudiants')
    return render(request, 'polls/saisie_etudiant.html')



def liste_etudiants(request):
    etudiants = Etudiant.objects.all()
    context = {'etudiants': etudiants}
    return render(request, 'polls/liste_etudiants.html', context)
